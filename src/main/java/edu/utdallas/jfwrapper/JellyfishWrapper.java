package edu.utdallas.jfwrapper;

import edu.utdallas.util.Utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Class used to interface with Jellyfish
 */
public class JellyfishWrapper {
    private JellyfishConfiguration config;

    public JellyfishWrapper(JellyfishConfiguration config) {
        this.config = config;
    }


    /**
     * @param inputFasta the input file to send to jellyfish count
     * @param output     the filename to write the output (just filename, not including directory)
     * @throws IOException
     * @throws InterruptedException
     */
    public void count(String inputFasta, String output) throws IOException, InterruptedException {
        StringBuilder command;
        if (Utilities.isCompressed(inputFasta)) {
            command = new StringBuilder("zcat ");
            command.append(inputFasta);
            command.append(" | jellyfish count");
        } else {
            command = new StringBuilder("jellyfish count");
        }

        command.append(" -m ");
        command.append(config.getMerLength());

        command.append(" -t ");
        command.append(config.getNumThreads());

        command.append(" -s ");
        command.append(config.getHashSize());


        String fullOutputName = Paths.get(config.getOutputDirectory(), output).toString();
        command.append(" -o ");
        command.append(fullOutputName);

        if (!Utilities.isCompressed(inputFasta)
        ) {
            command.append(" ");
            command.append(inputFasta);
        } else {
            // read from stdin (file descriptor 0)
            command.append(" ");
            command.append("/dev/fd/0");
        }

        Utilities.runCommand(command.toString());
    }


    /**
     * @param jellyfishDB
     * @param outputFile
     * @throws InterruptedException
     * @throws IOException
     */
    public void dump(String jellyfishDB, String outputFile) throws InterruptedException, IOException {
        // we want the output to be in two columns and to ignore kmers which occur only one time
        StringBuilder dumpCommand = new StringBuilder("jellyfish dump -t -c -L 2");

        String outputFilePath = Paths.get(config.getOutputDirectory(), outputFile).toString();
        dumpCommand.append(" -o ");
        dumpCommand.append(outputFilePath);

        String inputFilePath = Paths.get(config.getOutputDirectory(), jellyfishDB).toString();
        dumpCommand.append(" ");
        dumpCommand.append(inputFilePath);

        Utilities.runCommand(dumpCommand.toString());
    }


    /**
     * Display some statistics about the k-mers in the hash:
     * Unique:  Number  of  k-mers which occur only once.  Distinct: Number of
     * k-mers, not counting multiplicity.  Total: Number of k-mers,  including
     * multiplicity.  Max_count: Maximum number of occurrence of a k-mer.
     * for optional parameters, enter null to omit
     *
     * @param lowerCount
     * @param upperCount
     * @param verbose
     * @param jellyfishDB
     */
    public void stats(Integer lowerCount, Integer upperCount, boolean verbose, String jellyfishDB, String outputFile) throws IOException, InterruptedException {
        String command = "jellyfish stats";
        if (lowerCount != null) {
            command += " -L " + lowerCount.toString();
        }
        if (upperCount != null) {
            command += " -U " + upperCount.toString();
        }
        if (verbose) {
            command += "-v";
        }

        command += " -o " + outputFile + " " + jellyfishDB;

        Utilities.runCommand(command);
    }

    /**
     * returns the total k-mer count from the stats file
     *
     * @param file
     * @return long the total k-mer count
     */
    public long getTotalCount(String file, String tempDirectory) throws IOException, InterruptedException {
        long count = 0;
        String tempFilePath = Paths.get(tempDirectory, "tempStatsFile.txt").toString();
        stats(2, null, false, file, tempFilePath);

        File f = new File(tempFilePath);
        Scanner scan = new Scanner(f);

        count = -1;
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            String[] pieces = line.split("\\s");
            System.out.println(line);
            String field = pieces[0];
            if (field.equals("Total:")) {
                count = Long.parseLong(pieces[pieces.length - 1]);
            }
        }

        f.delete();
        return count;
    }
}
