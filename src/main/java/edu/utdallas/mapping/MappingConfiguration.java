package edu.utdallas.mapping;

import java.nio.file.Paths;

public class MappingConfiguration {
    private String outputDirectory;
    private String kmersFile;
    private int batchSize;
    private boolean useKmerSearching;

    public MappingConfiguration(String outputDirectory, String kmersFile, int batchSize, boolean useKmerSearching) {
        this.outputDirectory = outputDirectory;
        this.kmersFile = Paths.get(outputDirectory, kmersFile).toString();
        this.batchSize = batchSize;
        this.useKmerSearching = useKmerSearching;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public String getKmersFile() {
        return kmersFile;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public boolean isUseKmerSearching() {
        return useKmerSearching;
    }
}
