package edu.utdallas.util;

public class SubprocessException extends RuntimeException {
    private final String command;
    private final int exitCode;

    public SubprocessException(String command, String errorMsg, int exitCode) {
        super(String.format("%s exited with error code %d: %s", command, exitCode, errorMsg));
        this.command = command;
        this.exitCode = exitCode;
    }

    public String getCommand() {
        return command;
    }

    public int getExitCode() {
        return exitCode;
    }
}
