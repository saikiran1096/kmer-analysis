package edu.utdallas.seq;

public class SequencesPair {
    private Sequences normal;
    private Sequences tumor;

    public SequencesPair(Sequences normal, Sequences tumor) {
        this.normal = normal;
        this.tumor = tumor;
    }

    public Sequences getNormal() {
        return normal;
    }

    public Sequences getTumor() {
        return tumor;
    }
}
