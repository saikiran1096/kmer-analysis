package edu.utdallas.util;


import java.io.*;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by shayan on 2/18/18.
 */
public class Utilities {
    private Utilities() {
    }

    /**
     * Method normalizes and filters the data
     *
     * @param file             input file containing kmers and counts of both normal and tumor
     * @param filteredNoValues output file to store filtered kmers without counts
     * @param scaledUnfiltered output file to store unfiltered kmers along with their normalized counts
     * @param funs             filter functions to apply to kmers
     * @param normalCount      the kmer count in the normal sample
     * @param tumorCount       the kmer count in the tumor sample
     */
    public static void filterAndScale(String file, String filteredNoValues, String scaledUnfiltered, List<BiFunction<Double, Double, Boolean>> funs, long normalCount, long tumorCount, boolean tss) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(filteredNoValues));
        BufferedWriter writer2 = new BufferedWriter(new FileWriter(scaledUnfiltered));
        BufferedWriter writer3 = new BufferedWriter(new FileWriter(filteredNoValues + ".values"));
        String line = null;

        double largerCount = Math.max(normalCount, tumorCount);

        while ((line = reader.readLine()) != null) {
            String[] split = line.split(" ");
            String kmer = split[0];

            // Read and scale count of kmer for use in filtering
            double count = Integer.parseInt(split[1]) * 1.0 / normalCount;
            double count2 = Integer.parseInt(split[2]) * 1.0 / tumorCount;

            if (!tss) {
                count *= largerCount;
                count2 *= largerCount;
            }


            // Write kmer and counts to combined normal and tumor file
            writer2.write(String.format("%s %f %f%n", kmer, count, count2));

            boolean result = true;
            for (BiFunction<Double, Double, Boolean> fun : funs) {
                result &= fun.apply(count, count2);
            }

            if (result) {
                // Kmer passes all filters, write to filtered kmer file
                writer.write(kmer + "\n");
                writer3.write(String.format("%s %f %f%n", kmer, count, count2));
            }
        }
        writer.close();
        writer2.close();
        writer3.close();
    }

    /**
     * merges two sorted files containing kmer counts (i.e. tumor and normal) maintaining the sorted order by kmer
     *
     * @param file1  normal dump
     * @param file2  tumor dump
     * @param output combined file
     */
    public static void combineKmerCounts(String file1, String file2, String output) throws IOException {
        BufferedReader reader1 = new BufferedReader(new FileReader(file1));
        BufferedReader reader2 = new BufferedReader(new FileReader(file2));
        BufferedWriter writer = new BufferedWriter(new FileWriter(output));
        String kmer1, kmer2;
        int val1, val2;
        String line1, line2;
        String[] split1, split2;
        if (((line1 = reader1.readLine()) != null) && ((line2 = reader2.readLine()) != null)) {
            split1 = line1.split("\\s");
            split2 = line2.split("\\s");
            kmer1 = split1[0];
            kmer2 = split2[0];
            val1 = Integer.parseInt(split1[1]);
            val2 = Integer.parseInt(split2[1]);
            while (true) {
                int cmp = kmer1.compareTo(kmer2);
                if (cmp == 0) {
                    writer.write(kmer1 + " " + val1 + " " + val2 + "\n");
                    if (((line1 = reader1.readLine()) == null) || ((line2 = reader2.readLine()) == null))
                        break;
                    split1 = line1.split("\\s");
                    split2 = line2.split("\\s");
                    kmer1 = split1[0];
                    kmer2 = split2[0];
                    val1 = Integer.parseInt(split1[1]);
                    val2 = Integer.parseInt(split2[1]);
                } else if (cmp < 0) {
                    writer.write(kmer1 + " " + val1 + " 0\n");
                    if ((line1 = reader1.readLine()) != null) {
                        split1 = line1.split("\\s");
                        kmer1 = split1[0];
                        val1 = Integer.parseInt(split1[1]);
                    } else {
                        writer.write(kmer2 + " 0 " + val2 + "\n");
                        break;
                    }
                } else if (cmp > 0) {
                    writer.write(kmer2 + " 0 " + val2 + "\n");
                    if ((line2 = reader2.readLine()) != null) {
                        split2 = line2.split("\\s");
                        kmer2 = split2[0];
                        val2 = Integer.parseInt(split2[1]);
                    } else {
                        writer.write(kmer1 + " " + val1 + " 0\n");
                        break;
                    }
                }
            }
        }
        while ((line1 = reader1.readLine()) != null) {
            split1 = line1.split("\\s");
            kmer1 = split1[0];
            val1 = Integer.parseInt(split1[1]);
            writer.write(kmer1 + " " + val1 + " 0\n");
        }
        while ((line2 = reader2.readLine()) != null) {
            split2 = line2.split("\\s");
            kmer2 = split2[0];
            val2 = Integer.parseInt(split2[1]);
            writer.write(kmer2 + " 0 " + val2 + "\n");
        }
        writer.close();

    }

    /**
     * Run a command and return the outputs as a String
     *
     * @param command the command to run
     * @return the output for the program
     */
    public static String runCommand(String command) throws IOException, InterruptedException {
        System.out.println(command);

        File outputLog = File.createTempFile("kmers-subp-log", null);
        outputLog.deleteOnExit();

        ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
        pb.redirectErrorStream(true);
        pb.redirectOutput(ProcessBuilder.Redirect.appendTo(outputLog));
        Process p = pb.start();
        p.waitFor();

        try (BufferedReader reader = new BufferedReader(new FileReader(outputLog))) {
            Stream<String> stream = reader.lines();

            String output = stream.collect(Collectors.joining("\n"));

            if (p.exitValue() != 0) {
                throw new SubprocessException(command, output, p.exitValue());
            } else {
                return output;
            }
        }
    }

    /**
     * Split the sequences into R1 and R2. Input sequence file needs to be sorted by headers.
     * First occurrence should belong to R1, and the second occurrence to R2.
     *
     * @param sequencesFile the input file with sequences
     * @param R1output      the R1 file to write to
     * @param R2output      the R2 file to write to
     * @throws IOException
     */
    public static void splitSequences(String sequencesFile, String R1output, String R2output) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(sequencesFile));
        BufferedWriter writer1 = new BufferedWriter(new FileWriter(R1output));
        BufferedWriter writer2 = new BufferedWriter(new FileWriter(R2output));

        String original_header, line, header;


        original_header = reader.readLine();
        line = stripHeader(original_header);
        header = line;
        while (line != null) {
            for (int i = 0; i < 4; i++) {
                writer1.write(original_header);
                writer1.newLine();

                original_header = reader.readLine();
                line = stripHeader(original_header);
            }
            if (header.equals(line)) {
                for (int i = 0; i < 4; i++) {
                    writer2.write(original_header);
                    writer2.newLine();

                    original_header = reader.readLine();
                    line = stripHeader(original_header);
                }
            }
            header = line;
        }
        writer1.close();
        writer2.close();
        reader.close();

    }

    /**
     * Strips a header of characters after a forward slash or space character
     *
     * @param header The header to strip characters from
     * @return The stripped header
     */
    public static String stripHeader(String header) {
        if (header == null)
            return null;
        if (header.contains("/"))
            return header.substring(0, header.indexOf("/"));
        if (header.contains(" "))
            return header.substring(0, header.indexOf(" "));
        return header;
    }

    /**
     * Tests if the file with given name is compressed by checking the file extension.
     *
     * @param fileName name of the file
     * @return true if file is compressed, false otherwise
     */
    public static boolean isCompressed(String fileName) {
        return fileName.endsWith(".gz");
    }

    public static void combineReads(String read1, String read2, String outputFile) throws IOException, InterruptedException {
        File f = new File(outputFile);
        if (f.exists()) {
            f.delete();
            f.createNewFile();
        } else {
            f.createNewFile();
        }
        if (isCompressed(read1)) {
            runCommand(String.format("zcat %s >> %s", read1, outputFile));
        } else {
            runCommand(String.format("cat %s >> %s", read1, outputFile));
        }
        if (isCompressed(read2)) {
            runCommand(String.format("zcat %s >> %s", read2, outputFile));
        } else {
            runCommand(String.format("cat %s >> %s", read2, outputFile));
        }
    }
}
