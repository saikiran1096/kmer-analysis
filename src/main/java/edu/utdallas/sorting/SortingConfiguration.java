package edu.utdallas.sorting;

public class SortingConfiguration {

    private String bufferSize;
    private String outputDirectory;
    private int merLength;
    private boolean useKmerSorting;

    public SortingConfiguration(String outputDirectory, int merLength, String bufferSize, boolean useKmerSorting) {
        this.outputDirectory = outputDirectory;
        this.merLength = merLength;
        this.bufferSize = bufferSize;
        this.useKmerSorting = useKmerSorting;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public int getMerLength() {
        return merLength;
    }

    public String getBufferSize() {
        return bufferSize;
    }

    public boolean isUseKmerSorting() {
        return useKmerSorting;
    }
}
