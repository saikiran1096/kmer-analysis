package edu.utdallas.jfwrapper;

public class JellyfishConfiguration {
    private int merLength;
    private int numThreads;
    private String hashSize;
    private String outputDirectory;

    public JellyfishConfiguration(int merLength, int numThreads, String hashSize, String outputDirectory) {
        this.merLength = merLength;
        this.numThreads = numThreads;
        this.hashSize = hashSize;
        this.outputDirectory = outputDirectory;
    }

    public int getMerLength() {
        return merLength;
    }

    public int getNumThreads() {
        return numThreads;
    }

    public String getHashSize() {
        return hashSize;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }
}
