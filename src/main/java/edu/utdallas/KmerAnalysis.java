package edu.utdallas;


import edu.utdallas.jfwrapper.JellyfishConfiguration;
import edu.utdallas.jfwrapper.JellyfishWrapper;
import edu.utdallas.mapping.Mapper;
import edu.utdallas.mapping.MappingConfiguration;
import edu.utdallas.metrics.DifferenceMetrics;
import edu.utdallas.seq.Sequences;
import edu.utdallas.seq.SequencesPair;
import edu.utdallas.sorting.Sorter;
import edu.utdallas.sorting.SortingConfiguration;
import edu.utdallas.util.Utilities;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Contains the main method
 */
public class KmerAnalysis {
    //tracks how long the code takes to run
    private static long time = System.nanoTime();
    private static long startTime = System.nanoTime();

    private KmerAnalysis() {
    }

    private static void time() {
        System.out.printf("%f seconds elapsed since last checkpoint%n", (double) (System.nanoTime() - time) / 1_000_000_000);
        time = System.nanoTime();
    }

    private static SequencesPair parseSequenceArgs(List<String> files, boolean normalSplit, boolean tumorSplit) {
        int filesRequired = 2;
        if (normalSplit) {
            filesRequired += 1;
        }
        if (tumorSplit) {
            filesRequired += 1;
        }

        if (filesRequired != files.size()) {
            throw new IllegalArgumentException(String.format("Got %d files but expected %d. (Are --normalSplit and --tumorSplit set correctly?)",
                    files.size(), filesRequired));
        }

        Sequences normal;
        if (normalSplit) {
            normal = Sequences.splitFile(files.remove(0), files.remove(0));
        } else {
            normal = Sequences.combinedFile(files.remove(0));
        }

        Sequences tumor;
        if (tumorSplit) {
            tumor = Sequences.splitFile(files.remove(0), files.remove(0));
        } else {
            tumor = Sequences.combinedFile(files.remove(0));
        }

        return new SequencesPair(normal, tumor);

    }

    public static void main(String[] args) throws IOException, InterruptedException {
        ArgumentParser parser = ArgumentParsers.newFor("kmer_analysis").build()
                .defaultHelp(true);
        parser.addArgument("files")
                .help("Files containing sequences")
                .type(String.class)
                .nargs("+")
                .required(true);
        parser.addArgument("--normalSplit")
                .help("Normal reads stored in two separate files")
                .type(Boolean.class)
                .action(Arguments.storeTrue());
        parser.addArgument("--tumorSplit")
                .help("Tumor reads stored in two separate files")
                .type(Boolean.class)
                .action(Arguments.storeTrue());
        parser.addArgument("-o")
                .help("output directory")
                .type(String.class)
                .setDefault("data");
        parser.addArgument("-m")
                .help("mer size")
                .type(Integer.class)
                .setDefault(25);
        parser.addArgument("-s")
                .help("hash size for jellyfish")
                .type(String.class)
                .setDefault("100M");
        parser.addArgument("-S")
                .help("buffer size for sort")
                .type(String.class);
        parser.addArgument("--kms")
                .help("Use kmer-sorting to sort instead of GNU sort and use kmer-searcher for kmer mapping")
                .type(Boolean.class)
                .action(Arguments.storeTrue());
        parser.addArgument("--tss")
                .help("Use total-sum-scaling instead of scaling to the larger file")
                .type(Boolean.class)
                .action(Arguments.storeTrue());
        parser.addArgument("-t")
                .help("number of threads to use for jellyfish and mapping kmers to sequences")
                .type(Integer.class)
                .setDefault(10);
        parser.addArgument("-g")
                .help("grep pattern size")
                .type(Integer.class)
                .setDefault(1_000_000);
        parser.addArgument("-fMinN")
                .help("filter k-mers with less than 'FMINN' occurrences in sequences of normal genome")
                .type(Integer.class);
        parser.addArgument("-fMaxN")
                .help("filter k-mers with more than 'FMAXN' occurrences in sequences of normal genome")
                .type(Integer.class);
        parser.addArgument("-fMinT")
                .help("filter k-mers with less than 'FMINT' occurrences in sequences of tumor genome")
                .type(Integer.class);
        parser.addArgument("-fMaxT")
                .help("filter k-mers with more than 'FMAXT' occurrences in sequences of tumor genome")
                .type(Integer.class);
        parser.addArgument("-fRatio")
                .help("filter k-mers with for which tumor/normal < 'FRATIO'")
                .type(Double.class);
        parser.addArgument("-fDiff")
                .help("filter k-mers for which tumor-normal < 'FDIFF'")
                .type(Double.class);
        parser.addArgument("-fFold")
                .help("filter k-mers for which tumor/normal > 'FFOLD'")
                .type(Double.class);

        Namespace res = null;
        try {
            res = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }


        String outputDirectory = res.get("o");

        SequencesPair seqs = parseSequenceArgs(res.get("files"), res.get("--normalSplit"), res.get("--tumorSplit"));

        String normal;
        if (seqs.getNormal().isSplit()) {
            normal = Paths.get(outputDirectory, "normal-combined.fastq").toString();
            Utilities.combineReads(seqs.getNormal().getRead1(), seqs.getNormal().getRead2(), normal);
        } else {
            normal = seqs.getNormal().getFile();
        }

        String tumor;
        if (seqs.getTumor().isSplit()) {
            tumor = Paths.get(outputDirectory, "tumor-combined.fastq").toString();
            Utilities.combineReads(seqs.getTumor().getRead1(), seqs.getTumor().getRead2(), tumor);
        } else {
            tumor = seqs.getTumor().getFile();
        }

        int merSize = res.get("m");
        String hashSize = res.get("s");
        int numThreads = res.get("t");

        new File(outputDirectory).mkdirs();

        JellyfishConfiguration jfConfig = new JellyfishConfiguration(merSize, numThreads, hashSize, outputDirectory);
        JellyfishWrapper jellyfishWrapper = new JellyfishWrapper(jfConfig);

        jellyfishWrapper.count(normal, "normal.out");
        jellyfishWrapper.count(tumor, "tumor.out");

        System.out.println("Finished count");
        time();


        String bufferSize = res.get("S");
        boolean kmerSorting = res.get("kms");

        jellyfishWrapper.dump("normal.out", "normal.dump");
        jellyfishWrapper.dump("tumor.out", "tumor.dump");


        SortingConfiguration sortingConfiguration = new SortingConfiguration(outputDirectory, merSize, bufferSize, kmerSorting);
        Sorter sorter = new Sorter(sortingConfiguration);

        System.out.println("Finished dump");
        time();

        sorter.sortKmersFile("normal.dump", "normal.dump.sorted");
        sorter.sortKmersFile("tumor.dump", "tumor.dump.sorted");
        System.out.println("Finished sorting kmers");
        time();

        //combines sorted normal and tumor files
        Utilities.combineKmerCounts(
                Paths.get(outputDirectory, "normal.dump.sorted").toString(),
                Paths.get(outputDirectory, "tumor.dump.sorted").toString(),
                Paths.get(outputDirectory, "combined.unscaled.txt").toString()
        );
        System.out.println("Finished combining");
        time();


        List<BiFunction<Double, Double, Boolean>> filters = new ArrayList<>();

        //Filter A checks against a minimum normal threshold and a maximum tumor threshold
        int minT, maxT, minN, maxN;
        if (res.get("fMinT") != null) {
            minT = res.get("fMinT");
        } else {
            minT = Integer.MIN_VALUE;
        }
        if (res.get("fMaxT") != null) {
            maxT = res.get("fMaxT");
        } else {
            maxT = Integer.MAX_VALUE;
        }
        if (res.get("fMinN") != null) {
            minN = res.get("fMinN");
        } else {
            minN = Integer.MIN_VALUE;
        }
        if (res.get("fMaxN") != null) {
            maxN = res.get("fMaxN");
        } else {
            maxN = Integer.MAX_VALUE;
        }
        if (res.get("fMaxT") != null || res.get("fMinN") != null || res.get("fMinT") != null || res.get("fMaxN") != null) {
            System.out.println("Filter A - Max and Min");
            filters.add((x, y) -> x < maxN && x > minN && y > minT && y < maxT);
        }

        //Filter B checks against a fold change in which (tumor count)/(normal count) is below a threshold
        if (res.get("fRatio") != null) {
            double maxRatio = res.get("fRatio");
            System.out.println("Filter B - Ratio");
            filters.add((x, y) -> y / x < maxRatio);
        }

        //Filter C checks against a difference where (tumor count) - (normal count) is below a threshold
        if (res.get("fDiff") != null) {
            double diffMax = res.get("fDiff");
            System.out.println("Filter C - Difference Tumor - Normal");
            filters.add((x, y) -> Math.abs(y - x) < diffMax);
        }
        //Filter D checks against Fold Change of Tumor/Normal is above a threshold
        if (res.get("fFold") != null) {
            double foldChange = res.get("fFold");
            System.out.println("Filter D - Fold Change");
            filters.add((x, y) -> Math.abs(y / x) > foldChange);
        }


        //gets total count of kmers in normal and tumor files and calculates normalization factor
        long normalCount = jellyfishWrapper.getTotalCount(Paths.get(outputDirectory, "normal.out").toString(), outputDirectory);
        long tumorCount = jellyfishWrapper.getTotalCount(Paths.get(outputDirectory, "tumor.out").toString(), outputDirectory);
        boolean tss = res.get("tss");

        Utilities.filterAndScale(
                Paths.get(outputDirectory, "combined.unscaled.txt").toString(),
                Paths.get(outputDirectory, "filtered.txt").toString(),
                Paths.get(outputDirectory, "combined.txt").toString(),
                filters, normalCount, tumorCount, tss);
        System.out.println("Finished filtering");
        time();

        //RMSD
        double rmsd_value = DifferenceMetrics.rmsd(Paths.get(outputDirectory, "combined.txt").toString());
        System.out.println("RMSD = " + rmsd_value);
        time();

        double js = DifferenceMetrics.jensenShannon(Paths.get(outputDirectory, "combined.txt").toString(), Math.max(normalCount, tumorCount));
        System.out.println("Jensen-Shannon = " + js);
        time();

        MappingConfiguration mappingConfiguration = new MappingConfiguration(outputDirectory, "filtered.txt", res.get("g"), kmerSorting);
        Mapper mapper = new Mapper(mappingConfiguration);

        mapper.mapKmersToHeaders(normal, "normal-headers.txt");

        System.out.println("Finished mapping kmers to normal headers");
        time();

        mapper.mapHeadersToSequences("normal-headers.txt", normal, "normal-sequences.txt");

        System.out.println("Finished mapping normal headers to sequences");
        time();

        mapper.mapKmersToHeaders(tumor, "tumor-headers.txt");

        System.out.println("Finished mapping tumor kmers to headers");
        time();

        mapper.mapHeadersToSequences("tumor-headers.txt", tumor, "tumor-sequences.txt");

        System.out.println("Finished mapping tumor headers to sequences");
        time();

        Utilities.splitSequences(Paths.get(outputDirectory, "tumor-sequences.txt").toString(),
                Paths.get(outputDirectory, "tumor-sequences.R1.txt").toString(),
                Paths.get(outputDirectory, "tumor-sequences.R2.txt").toString());

        Utilities.splitSequences(Paths.get(outputDirectory, "normal-sequences.txt").toString(),
                Paths.get(outputDirectory, "normal-sequences.R1.txt").toString(),
                Paths.get(outputDirectory, "normal-sequences.R2.txt").toString());

        System.out.println("Finished searching");
        time();

        System.out.printf("%f seconds total%n", (double) (System.nanoTime() - startTime) / 1_000_000_000);
    }

}
