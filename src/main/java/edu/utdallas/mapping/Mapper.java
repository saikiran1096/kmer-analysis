package edu.utdallas.mapping;

import edu.utdallas.util.SubprocessException;
import edu.utdallas.util.Utilities;

import java.io.*;
import java.nio.file.Paths;

public class Mapper {
    private MappingConfiguration config;

    public Mapper(MappingConfiguration config) {
        this.config = config;
    }

    public void mapKmersToHeaders(String sequences, String outputName) throws IOException, InterruptedException {
        // Split the kmers file file into chunks of 'numLines' lines each with each chunk filename
        // the prefix "kmers"
        String outputFile = Paths.get(config.getOutputDirectory(), outputName).toString();
        String splitCommand = String.format("split -l %d %s %s/kmers",
                config.getBatchSize(), config.getKmersFile(), config.getOutputDirectory());
        Utilities.runCommand(splitCommand);
        File[] files = new File(config.getOutputDirectory()).listFiles();

        // Create an empty file in the output directory named 'unsortedHeaders.txt'
        Utilities.runCommand("cat /dev/null > " + config.getOutputDirectory() + "/unsortedHeaders.txt");

        for (File f : files) {
            if (f.getName().startsWith("kmers")) {
                // Search for the kmers from each chunk in the sequences input file
                // and place the headers in 'unsortedHeaders.txt'
                // This command pipes a call to grep into another call to grep
                // The first grep grabs the sequence containing the kmer and the header associated with it
                // The second grep captures only the header by keeping only the line starting with '@'
                try {
                    String mappingCommand;
                    if (config.isUseKmerSearching()) {
                        if (Utilities.isCompressed(sequences)) {
                            mappingCommand = String.format("zcat %s | kmer-searcher %s /dev/fd/0 - 1", sequences, f.toString());
                        } else {
                            mappingCommand = String.format("kmer-searcher %s %s - 1", f.toString(), sequences);
                        }
                    } else {
                        if (Utilities.isCompressed(sequences)) {
                            mappingCommand = String.format("zcat %s | grep -F -B 1 --no-group-separator -f %s | grep @", sequences, f.toString());
                        } else {
                            mappingCommand = String.format("grep -F -B 1  --no-group-separator -f %s %s | grep @", f.toString(), sequences);
                        }
                    }
                    String outputCommand = String.format("%s >> %s/unsortedHeaders.txt", mappingCommand, config.getOutputDirectory());
                    Utilities.runCommand(outputCommand);

                } catch (SubprocessException e) {
                    // handle grep returning exit code 1 if no matches are found
                    if (e.getExitCode() != 1) {
                        throw e;
                    }
                }
                // Remove chunk of kmers after we are done searching for them
                f.delete();
            }
        }

        File f = new File(Paths.get(config.getOutputDirectory(), "unsortedStrippedHeaders").toString());
        BufferedWriter writer = new BufferedWriter(new FileWriter(f));
        BufferedReader reader = new BufferedReader(new FileReader(config.getOutputDirectory() + "/unsortedHeaders.txt"));

        String line;
        while ((line = reader.readLine()) != null) {
            writer.write(Utilities.stripHeader(line));
            writer.write("\n");
        }
        reader.close();
        writer.close();


        // Since a single sequence may contain multiple kmers which passed the filters,
        // we make a call to sort in order to remove duplicates
        Utilities.runCommand("sort --parallel=$(nproc) -S 50% -u " + f.getAbsolutePath() + " > " + outputFile);

        // Remove intermediary files
        f.delete();
        Utilities.runCommand("rm " + config.getOutputDirectory() + "/unsortedHeaders.txt");
    }

    public void mapHeadersToSequences(String headersName, String sequences, String outputName) throws InterruptedException, IOException {
        // Split the kmers file file into chunks of 'numLines' lines each with each chunk filename
        // the prefix "headers"
        String headers = Paths.get(config.getOutputDirectory(), headersName).toString();
        String output = Paths.get(config.getOutputDirectory(), outputName).toString();
        Utilities.runCommand("split -l " + config.getBatchSize() + " " + headers + " " + config.getOutputDirectory() + "/headers");
        File[] files = new File(config.getOutputDirectory()).listFiles();
        String unsortedSequencesPath = Paths.get(config.getOutputDirectory(), "unsortedSequences.txt").toString();
        Utilities.runCommand("cat /dev/null > " + unsortedSequencesPath);
        for (File f : files) {
            // There is opportunity to rewrite this to run the calls to grep in parallel
            // This would require more RAM, and we should be careful when writing to 'unsortedSequences.txt'
            if (f.getName().startsWith("headers")) {
                // Search for the headers from each chunk in the sequences input file
                // and place the sequences in 'unsortedSequences.txt'
                // The grep grabs the entire 4 line fasta sequence associated with each header
                String searchCommand;
                if (Utilities.isCompressed(sequences)) {
                    searchCommand = String.format("zcat %s | grep -F -A 3  --no-group-separator -f %s >> %s", sequences, f.toString(), unsortedSequencesPath);
                } else {
                    searchCommand = String.format("grep -F -A 3  --no-group-separator -f %s %s >> %s", f.toString(), sequences, unsortedSequencesPath);
                }

                Utilities.runCommand(searchCommand);
                // Remove chunk of headers after we are done searching for them
                f.delete();
            }
        }

        // This command chains calls to paste, sort, and tr to sort the sequences by header
        // The call to writes each 4-line fasta sequence to a single line by replacing the newline characters with tabs
        // We then sort (by the header)
        // Next tr converts the single-line representation of the fasta sequences to the 4-line version
        // by replacing the tabs with newline characters
        Utilities.runCommand("paste - - - - < " + config.getOutputDirectory() + "/unsortedSequences.txt | sort --stable --parallel=$(nproc) -S 50% | tr '\\t' '\\n' > " + output);
        Utilities.runCommand("rm " + config.getOutputDirectory() + "/unsortedSequences.txt");
    }
}
