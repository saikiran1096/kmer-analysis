**Dependencies:**
- Java 8
- [Maven](https://maven.apache.org/download.cgi)
- [Jellyfish](https://github.com/gmarcais/Jellyfish)
- GNU Coreutils
- GNU Bash
- GNU Grep


**Optional Dependencies**
- [k-mer searcher](https://gitlab.com/saikiran1096/kmer-searcher)
- [k-mer sorter](https://gitlab.com/saikiran1096/kmer-sorting)

**Maven dependencies:**

* [Argeparse4j](https://argparse4j.github.io/)

**Usage Instructions**
1. Ensure all dependencies are installed
2. Clone the project:

    `git clone git@gitlab.com:saikiran1096/kmer-analysis.git`

3. Build the executable jar bundled with dependencies: 
    
    `mvn clean compile assembly:single`

    The jar will be located in the target/ directory
4. For information on how to the run the jar:

    `java -jar kmer-analyzer-1.0-jar-with-dependencies.jar -h`
