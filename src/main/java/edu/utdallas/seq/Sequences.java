package edu.utdallas.seq;

public class Sequences {
    final boolean split;

    final private String file;

    final private String read1;
    final private String read2;

    private Sequences(String file) {
        this.split = false;
        this.file = file;
        this.read1 = null;
        this.read2 = null;
    }

    private Sequences(String read1, String read2) {
        this.split = true;
        this.read1 = read1;
        this.read2 = read2;
        this.file = null;
    }

    public static Sequences splitFile(String read1, String read2) {
        return new Sequences(read1, read2);
    }

    public static Sequences combinedFile(String file) {
        return new Sequences(file);
    }

    public boolean isSplit() {
        return split;
    }

    public String getFile() {
        if (isSplit()) {
            throw new IllegalStateException("Sequence reads are stored in separate files");
        }
        return file;
    }

    public String getRead1() {
        if (!isSplit()) {
            throw new IllegalStateException("Sequence reads are not stored in separate files");
        }
        return read1;
    }

    public String getRead2() {
        if (!isSplit()) {
            throw new IllegalStateException("Sequence reads are not stored in separate files");
        }
        return read2;
    }
}
