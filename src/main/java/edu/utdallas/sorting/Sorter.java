package edu.utdallas.sorting;

import edu.utdallas.util.Utilities;

import java.io.IOException;
import java.nio.file.Paths;

public class Sorter {

    SortingConfiguration config;

    public Sorter(SortingConfiguration config) {
        this.config = config;
    }

    public void sortKmersFile(String inputFile, String outputFile) throws IOException, InterruptedException {
        String sortCommand;
        inputFile = Paths.get(config.getOutputDirectory(), inputFile).toString();
        outputFile = Paths.get(config.getOutputDirectory(), outputFile).toString();

        if (config.isUseKmerSorting()) {
            if (config.getBufferSize() == null)
                sortCommand = String.format("kmer-sorting %d %s %s %d", config.getMerLength(), inputFile, outputFile, 0);
            else
                sortCommand = String.format("kmer-sorting %d %s %s %s", config.getMerLength(), inputFile, outputFile, config.getBufferSize());
        } else {
            String bufferOption;
            if (config.getBufferSize() == null) {
                bufferOption = "";
            } else {
                bufferOption = "-S " + config.getBufferSize();
            }
            sortCommand = "sort --parallel=$(nproc) " + bufferOption + " -o  " + outputFile + " " + inputFile;
        }
        Utilities.runCommand(sortCommand);
    }
}
