package edu.utdallas.metrics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Contains methods to compute difference metrics between two sequence files
 */
public class DifferenceMetrics {
    private DifferenceMetrics() {
    }

    /**
     * Given the combined file, this method calculates the root mean square deviation between the normal and tumor files.
     *
     * @param filePath the combined file path with lines having the format: kmer normal_count tumor_count
     * @return the RMSD distance between the two files.
     */
    public static double rmsd(String filePath) throws IOException {
        double rmsd = 0;
        int counter = 0;

        BufferedReader read = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = read.readLine()) != null) {
            String[] split = line.split(" ");
            double normalCount = Double.parseDouble(split[1]);
            double tumorCount = Double.parseDouble(split[2]);
            rmsd += Math.pow(normalCount - tumorCount, 2);
            counter++;
        }

        rmsd = Math.sqrt(rmsd / counter);


        return rmsd;

    }


    /**
     * Compute the Jensen Shannon divergence
     *
     * @param filePath        the combined file
     * @param totalTumorCount total tumor kmer count
     * @return the divergence
     * @throws IOException
     */
    public static double jensenShannon(String filePath, long totalTumorCount) throws IOException {

        double js = 0;
        int counter = 0;

        BufferedReader read = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = read.readLine()) != null) {
            String[] split = line.split(" ");
            double normalCount = Double.parseDouble(split[1]) + 1;
            double tumorCount = Double.parseDouble(split[2]) + 1;
            double m = (normalCount + tumorCount) / 2.;
            js -= normalCount * Math.log(m / normalCount) + tumorCount * Math.log(m / tumorCount);
            counter++;
        }
        js /= 2 * (totalTumorCount + counter);


        return js;

    }
}
